package pointsystem.rest;

import pointsystem.ejb.ConsultaDAO;
import pointsystem.model.Concepto;
import pointsystem.model.UsoPuntos;
import pointsystem.model.Request.ConsultaConceptoRequest;
import pointsystem.model.Request.ConsultaFechaRequest;
import pointsystem.model.Request.ConsultaClienteRequest;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;
import java.util.List;

@Path("consulta")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ConsultasRest {

    @Inject
    ConsultaDAO consultaDAO;

    @POST
    @Path("/concepto")
    public Response agregarConsulta(ConsultaConceptoRequest r) {
        List<Concepto> uConceptos = consultaDAO.obtenerConceptos(r);
        return Response.ok(uConceptos).build();
    }

    @POST
    @Path("/fechaUso")
    public Response agregarConsulta(ConsultaFechaRequest w) {
        List<UsoPuntos> ufecha = consultaDAO.obtenerFechaUso(w);
        return Response.ok(ufecha).build();
    }

    @POST
    @Path("/cliente")
    public Response agregarConsulta(ConsultaClienteRequest p) {
        List<UsoPuntos> unombre = consultaDAO.obtenerNombreUso(p);
        return Response.ok(unombre).build();
    }
}
