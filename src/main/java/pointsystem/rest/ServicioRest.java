package pointsystem.rest;
import pointsystem.ejb.ServiciosDAO;
import pointsystem.model.Bolsa;
import pointsystem.model.UsoPuntosDetalle;
import pointsystem.model.Request.CargaPuntosRequest;
import pointsystem.model.Request.UtilizarPuntosRequest;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import java.util.List;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.Consumes;

@Path("servicios")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ServicioRest {

    
    
    @Inject
    ServiciosDAO serviciosDAO;
    @POST
    @Path("/carga-de-puntos")
    public Response cargar(CargaPuntosRequest request) {

        // Aquí puedes hacer lo que necesites con las reglas obtenidas
        // Por ejemplo, puedes devolverlas como parte de la respuesta
        Bolsa bolsa = this.serviciosDAO.cargar(request);

        return Response.ok(bolsa).build();
    }

    @POST
    @Path("/utilizar-puntos")
    public Response utilizar(UtilizarPuntosRequest request) {
        List<UsoPuntosDetalle> cantidadBolsas = this.serviciosDAO.untilizarPuntos(request);
        return Response.ok(cantidadBolsas).build();
    }
}