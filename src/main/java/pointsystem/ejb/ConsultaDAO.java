package pointsystem.ejb;

import pointsystem.model.Cliente;
import pointsystem.model.Concepto;
import pointsystem.model.UsoPuntos;
import pointsystem.model.Request.ConsultaConceptoRequest;
import pointsystem.model.Request.ConsultaFechaRequest;
import pointsystem.model.Request.ConsultaClienteRequest;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import java.math.BigInteger;
import java.util.List;

@Stateless
public class ConsultaDAO {
    @PersistenceContext(unitName = "point.systemPersistenceUnit")
    private EntityManager entityManager;


    public List<Concepto> obtenerConceptos(ConsultaConceptoRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Concepto> criteriaQuery = criteriaBuilder.createQuery(Concepto.class);
        Root<Concepto> root = criteriaQuery.from(Concepto.class);
        request.getMotivo();

        Predicate predicate = criteriaBuilder.equal(root.get("descripcion"), request.getMotivo());

        criteriaQuery.select(root).where(predicate);

        return entityManager.createQuery(criteriaQuery).getResultList();
    }
    public List<UsoPuntos> obtenerFechaUso(ConsultaFechaRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UsoPuntos> criteriaQuery = criteriaBuilder.createQuery(UsoPuntos.class);
        Root<UsoPuntos> root = criteriaQuery.from(UsoPuntos.class);
        request.getFecha();

        criteriaQuery.select(root.get("fecha"));

        criteriaQuery.where(criteriaBuilder.equal(root.get("fecha"), request.getFecha()));

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public List<UsoPuntos> obtenerNombreUso(ConsultaClienteRequest request) {
    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<UsoPuntos> criteriaQuery = criteriaBuilder.createQuery(UsoPuntos.class);
    Root<UsoPuntos> root = criteriaQuery.from(UsoPuntos.class);
    BigInteger idCliente = request.getIdCliente();
    Cliente cliente = entityManager.find(Cliente.class, idCliente);
    Predicate predicate = criteriaBuilder.equal(root.get("cliente"), cliente);

    criteriaQuery.select(root).where(predicate);

    return entityManager.createQuery(criteriaQuery).getResultList();

    }
}