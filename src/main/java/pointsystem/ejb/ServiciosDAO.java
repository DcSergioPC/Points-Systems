package pointsystem.ejb;

import pointsystem.model.Bolsa;
import pointsystem.model.Cliente;
import pointsystem.model.Concepto;
import pointsystem.model.Regla;
import pointsystem.model.UsoPuntos;
import pointsystem.model.UsoPuntosDetalle;
import pointsystem.model.Request.CargaPuntosRequest;
import pointsystem.model.Request.UtilizarPuntosRequest;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

@Stateless
public class ServiciosDAO {
    @PersistenceContext(unitName = "point.systemPersistenceUnit")
    private EntityManager entityManager;

    public Bolsa cargar (CargaPuntosRequest request) {
        
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Regla> criteriaQuery = criteriaBuilder.createQuery(Regla.class);
        Root<Regla> root = criteriaQuery.from(Regla.class);
        // Construye la condición where para que el monto esté entre el límite inferior y superior
        BigInteger monto = request.getMonto();
        criteriaQuery.where(
            criteriaBuilder.and(
                criteriaBuilder.lessThanOrEqualTo(root.get("limiteInf"), monto),
                criteriaBuilder.greaterThanOrEqualTo(root.get("limiteSup"), monto)
            )
        );

        List<Regla> reglas = entityManager.createQuery(criteriaQuery).getResultList();
        if(reglas.isEmpty()) {
            return null;
        }
        Regla regla = reglas.get(0);
        Bolsa bolsa = new Bolsa();
        bolsa.setCliente(regla.getCliente());
        bolsa.setUtilizado(BigInteger.ZERO);
        bolsa.setPuntaje(request.getMonto().divide(regla.getMonto()));
        bolsa.setSaldo(bolsa.getPuntaje());
        bolsa.setFechaAsignacion(LocalDate.now());
        bolsa.setFechaCaducidad(LocalDate.now().plusDays(30));
        bolsa.setMonto(request.getMonto());
        entityManager.persist(bolsa);

        return bolsa;
    }

    public List<UsoPuntosDetalle> untilizarPuntos(UtilizarPuntosRequest request) {
        Concepto concepto = this.entityManager.find(Concepto.class, request.getIdConcepto());
        BigInteger puntos = concepto.getPuntos();

        BigInteger idCliente = request.getIdCliente();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Bolsa> criteriaQuery = criteriaBuilder.createQuery(Bolsa.class);
        Root<Bolsa> root = criteriaQuery.from(Bolsa.class);

        criteriaQuery.select(root)
                    .where(
                        criteriaBuilder.and(
                            criteriaBuilder.equal(root.get("idCliente").get("idCliente"), idCliente),
                            criteriaBuilder.greaterThan(root.get("saldo"), 0)
                        )
                    )
                    .orderBy(criteriaBuilder.asc(root.get("idBolsa"))); // Orden ascendente por idBolsa

        
        List<Bolsa> bolsas = entityManager.createQuery(criteriaQuery).getResultList();
        BigInteger puntosUtilizadosCompletamente = BigInteger.valueOf(-1);
        List<UsoPuntosDetalle> listUsoPuntosDetalles = new java.util.ArrayList<>();
        UsoPuntos usoPuntos = new UsoPuntos(
            this.entityManager.find(Cliente.class, idCliente),
            puntos,
            LocalDate.now(),
            concepto
        );
        for (Bolsa bolsa : bolsas) {
            if (puntos.compareTo(BigInteger.ZERO) > 0) { // Verificar si aún quedan puntos por utilizar
                if (bolsa.getSaldo().compareTo(puntos) >= 0) {
                    bolsa.setSaldo(bolsa.getSaldo().subtract(puntos));
                    bolsa.setUtilizado(bolsa.getUtilizado().add(puntos));
                    UsoPuntosDetalle usoPuntosDetalle = new UsoPuntosDetalle(
                        bolsa.getCliente(),
                        usoPuntos,
                        puntos,
                        LocalDate.now(),
                        bolsa
                    );
                    listUsoPuntosDetalles.add(usoPuntosDetalle);
                    puntos = BigInteger.ZERO;
                    puntosUtilizadosCompletamente = bolsa.getIdBolsa(); // Todos los puntos se han utilizado
                } else {
                    puntos = puntos.subtract(bolsa.getSaldo());
                    bolsa.setUtilizado(bolsa.getUtilizado().add(bolsa.getSaldo()));
                    UsoPuntosDetalle usoPuntosDetalle = new UsoPuntosDetalle(
                        bolsa.getCliente(),
                        usoPuntos,
                        bolsa.getSaldo(),
                        LocalDate.now(),
                        bolsa
                    );
                    listUsoPuntosDetalles.add(usoPuntosDetalle);
                    bolsa.setSaldo(BigInteger.ZERO);
                }
            } else {
                break; // Salir del bucle si ya no quedan puntos por utilizar
            }
        }

        if (puntosUtilizadosCompletamente != BigInteger.valueOf(-1)) {
            this.entityManager.merge(usoPuntos);

            for (Bolsa bolsa : bolsas) {
                this.entityManager.merge(bolsa); // Aplicar los cambios solo si todos los puntos se utilizaron completamente
                if (bolsa.getIdBolsa().equals(puntosUtilizadosCompletamente)) {
                    break;
                }
            }

            for (UsoPuntosDetalle usoPuntosDetalle : listUsoPuntosDetalles) {
                this.entityManager.merge(usoPuntosDetalle);
            }
        }
        return listUsoPuntosDetalles;
    }
    
}
