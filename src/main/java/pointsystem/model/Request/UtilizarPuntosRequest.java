package pointsystem.model.Request;

import java.math.BigInteger;

public class UtilizarPuntosRequest {
    private BigInteger idCliente;
    private BigInteger idConcepto;

    public BigInteger getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(BigInteger idCliente) {
        this.idCliente = idCliente;
    }

    public BigInteger getIdConcepto() {
        return idConcepto;
    }

    public void setIdConcepto(BigInteger idConcepto) {
        this.idConcepto = idConcepto;
    }
}
