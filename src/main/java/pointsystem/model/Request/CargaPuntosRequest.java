package pointsystem.model.Request;

import java.math.BigInteger;

public class CargaPuntosRequest {
    private BigInteger idCliente;
    private BigInteger monto;

    public BigInteger getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(BigInteger idCliente) {
        this.idCliente = idCliente;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }
}
