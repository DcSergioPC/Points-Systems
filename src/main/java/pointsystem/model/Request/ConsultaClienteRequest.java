package pointsystem.model.Request;
import java.math.BigInteger;
public class ConsultaClienteRequest {

    private BigInteger idCliente;

    public BigInteger getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(BigInteger idCliente) {
        this.idCliente = idCliente;
    }

}