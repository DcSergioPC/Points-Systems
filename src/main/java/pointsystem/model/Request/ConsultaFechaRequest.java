package pointsystem.model.Request;
public class ConsultaFechaRequest {

    private java.time.LocalDate fecha;

    public java.time.LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(java.time.LocalDate fecha) {
        this.fecha = fecha;
    }

}