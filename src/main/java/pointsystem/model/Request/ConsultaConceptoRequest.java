package pointsystem.model.Request;
public class ConsultaConceptoRequest {
    private String motivo;

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

}