package pointsystem.model;

import java.math.BigInteger;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

// Detalle: id autogenerado, identificador de la cabecera, puntaje utilizado, identificador
// de la bolsa de puntos utilizada

@Entity
@Table(name = "usoPuntosDetalle")
public class UsoPuntosDetalle {
    @Id
    @Basic(optional = false)
    @Column(name = "id_usoPuntosDetalle")
    @GeneratedValue(generator = "usoPuntosDetalleSec")
    @SequenceGenerator(name = "usoPuntosDetalleSec",sequenceName = "usoPuntosDetalle_sec",allocationSize = 0)
    private BigInteger idUsoPuntosDetalle;

    @ManyToOne(optional = false)
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    private Cliente cliente;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "id_usoPuntos", referencedColumnName = "id_usoPuntos")
    private UsoPuntos idUsoPuntos;

    @Basic(optional = false)
    @Column(name = "puntosUtilizados")
    private java.math.BigInteger puntosUtilizados;
    
    @Basic(optional = false)
    @Column(name = "fecha")
    private java.time.LocalDate fecha;

    @ManyToOne(optional = false)
    @JoinColumn(name = "id_bolsa", referencedColumnName = "id_bolsa")
    private Bolsa idBolsa;

    public UsoPuntosDetalle() {
        // Constructor sin argumentos
    }
    public UsoPuntosDetalle (
            Cliente cliente,
            UsoPuntos idUsoPuntos,
            java.math.BigInteger puntosUtilizados,
            java.time.LocalDate fecha,
            Bolsa idBolsa
        ) {
        this.cliente = cliente;
        this.idUsoPuntos = idUsoPuntos;
        this.puntosUtilizados = puntosUtilizados;
        this.fecha = fecha;
        this.idBolsa = idBolsa;
    }

    public BigInteger getIdUsoPuntosDetalle() {
        return idUsoPuntosDetalle;
    }

    public void setIdUsoPuntosDetalle(BigInteger idUsoPuntosDetalle) {
        this.idUsoPuntosDetalle = idUsoPuntosDetalle;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public java.math.BigInteger getPuntosUtilizados() {
        return puntosUtilizados;
    }

    public void setPuntosUtilizados(java.math.BigInteger puntosUtilizados) {
        this.puntosUtilizados = puntosUtilizados;
    }

    public java.time.LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(java.time.LocalDate fecha) {
        this.fecha = fecha;
    }

    public Bolsa getIdBolsa() {
        return idBolsa;
    }

    public void setIdBolsa(Bolsa idBolsa) {
        this.idBolsa = idBolsa;
    }

    public UsoPuntos getIdUsoPuntos() {
        return idUsoPuntos;
    }

    public void setIdUsoPuntos(UsoPuntos idUsoPuntos) {
        this.idUsoPuntos = idUsoPuntos;
    }
}